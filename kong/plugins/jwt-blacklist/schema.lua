local typedefs = require "kong.db.schema.typedefs"

return {
    name = "jwt-blacklist",
    fields = {
        { protocols = typedefs.protocols_http },
        { config = {
            type = "record",
            fields = {
                { uri_param_name = {
                    type = "string",
                    default = "access_token",
                }, },
                { key_claim_name = { type = "string", default = "client_id" }, },
                { secret_is_base64 = { type = "boolean", default = false }, },
                { add_header_userid = { type = "boolean", default = true }, },
                { clear_header_jwt = { type = "boolean", default = false }, },
                { anonymous = { type = "string" }, },
                { run_on_preflight_OPTIONS_request = { type = "boolean", default = true }, },
                { maximum_expiration = {
                    type = "number",
                    default = 0,
                    between = { 0, 31536000 },
                }, },
                { header_name = {
                    type = "string",
                    default = "Authorization",
                }, },
                { redis_host = {
                    type = "string",
                    required = true,
                    default = "127.0.0.1"
                } },
                { redis_port = {
                    type = "number",
                    required = true,
                    default = 6379
                } },
                { redis_db = {
                    type = "number",
                    required = true,
                    default = 0
                } },
                { redis_password = {
                    type = "string",
                } },
                { blacklist_string_key_prefix = {
                    type = "string",
                    required = true,
                    default = "userOffline:"
                } },
            },
        },
        },
    },
    entity_checks = {
        { conditional = {
            if_field = "config.maximum_expiration",
            if_match = { gt = 0 },
            then_field = "config.claims_to_verify",
            then_match = { contains = "exp" },
        }, },
    },
}
