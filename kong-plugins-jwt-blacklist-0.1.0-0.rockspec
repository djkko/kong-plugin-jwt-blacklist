package = "kong-plugins-jwt-blacklist"
version = "0.1.0-0"
source = {
   url = "https://gitee.com/nullpointerxyz/kong-plugin-jwt-blacklist.git"
}
description = {
   homepage = "https://gitee.com/nullpointerxyz/kong-plugin-jwt-blacklist",
   license = "Apache 2.0"
}
build = {
   type = "builtin",
   modules = {
      ["kong.plugins.jwt-blacklist.asn_sequence"] = "kong/plugins/jwt-blacklist/asn_sequence.lua",
      ["kong.plugins.jwt-blacklist.handler"] = "kong/plugins/jwt-blacklist/handler.lua",
      ["kong.plugins.jwt-blacklist.jwt_parser"] = "kong/plugins/jwt-blacklist/jwt_parser.lua",
      ["kong.plugins.jwt-blacklist.schema"] = "kong/plugins/jwt-blacklist/schema.lua"
   }
}
